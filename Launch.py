
import first_menu
import main_game

game_mode = "menu"

while game_mode != 'quit':
    
    if game_mode == "menu":
        game_mode = first_menu.intro_screen()
    else:
        game_mode = main_game.main(game_mode)
