########################## Setup

import pygame
import pygame.freetype
import pygame.time
from time import time
pygame.init()

########################## Create text

fontfile = {}
fontfile["50_True"] = pygame.freetype.SysFont('Comic Sans MS', 50, True)
fontfile["40_True"] = pygame.freetype.SysFont('Comic Sans MS', 40, True)
fontfile["40_False"] = pygame.freetype.SysFont('Comic Sans MS', 40, False)
fontfile["30_False"] = pygame.freetype.SysFont('Comic Sans MS', 30, False)
fontfile["30_True"] = pygame.freetype.SysFont('Comic Sans MS', 30, True)
fontfile["20_False"] = pygame.freetype.SysFont('Comic Sans MS', 20, False)
fontfile["20_True"] = pygame.freetype.SysFont('Comic Sans MS', 20, True)
fontfile["15_False"] = pygame.freetype.SysFont('Comic Sans MS', 15, False)
fontfile["15_True"] = pygame.freetype.SysFont('Comic Sans MS', 15, True)
fontfile["10_False"] = pygame.freetype.SysFont('Comic Sans MS', 10, False)

def create_button(image, text, xy, size, bold = False):
    global fontfile
    font = fontfile[str(size) + "_" + str(bold)]
    font.render_to(image, xy, text)
    
########################## Intro screen

def intro_screen():
    
    def check_slot(filename, slot, xy):
        try:
            f = open(filename)
            f.close()
            game_mode = slot
            done = True
        except:
            create_button(background, "Slot empty!", xy, 30)
            game_mode = None
            done = False
        return game_mode, done
            
    
    ########################## Screen
    width = 650
    height = 650
    resulutsioon = (width, height)
    screen = pygame.display.set_mode(resulutsioon, pygame.RESIZABLE)
    pygame.display.set_caption("Mõõgamäng 0.1.0")
    background_original = pygame.image.load("opening.png").convert()
    
    ########################## Loop
    
    done = False
    page = "front"
    endtime = 1
    starttime = 0

    while not done:
        
    ########################## Fps calc + Fps timer start
        
        fps = round(1/(endtime - starttime), 2)
        starttime = time()
        
    ########################## Where to write text
        
        background = pygame.Surface.copy(background_original)
        
        
    ########################## Show fps
        if fps > 60:
            pygame.time.Clock().tick(60)
            fps = 60
        create_button(background, str(fps), (5, 5), 10)
        
    ########################## To quit
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_mode = "quit"
                done = True
                
    ########################## Front page
        if page == "front":  
            new_game = False
            load_game = False
            quit_game = False
            mouse_pointer = pygame.mouse.get_pos()
            if mouse_pointer[0] in range(230, 425) and mouse_pointer[1] in range(190, 225):
                new_game = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    game_mode = "new game"
                    done = True
                    pygame.time.wait(200)
            elif mouse_pointer[0] in range(220, 425) and mouse_pointer[1] in range(300, 350):
                load_game = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    page = "load"
                    pygame.time.wait(200)
            elif mouse_pointer[0] in range(230, 310) and mouse_pointer[1] in range(500, 540):
                quit_game = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    done = True
                    game_mode = "quit"
                    pygame.time.wait(200)
                    print("Closing")
            
            create_button(background, "New Game", (230, 200), 40, new_game)
            create_button(background, "Load Game", (230, 300), 40, load_game)
            create_button(background, "Quit", (230, 500), 40, quit_game)

    ########################## Load page
        elif page == "load":
            slot1 = False
            slot2 = False
            slot3 = False
            back = False
            mouse_pointer = pygame.mouse.get_pos()
            if mouse_pointer[0] in range(250, 360) and mouse_pointer[1] in range(200, 230):
                slot1 = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    result = check_slot('slot1.txt', 'slot1', (450,200))
                    game_mode = result[0]
                    done = result[1]
            elif mouse_pointer[0] in range(250, 360) and mouse_pointer[1] in range(300, 330):
                slot2 = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    result = check_slot('slot2.txt', 'slot2', (450,300))
                    game_mode = result[0]
                    done = result[1]
            elif mouse_pointer[0] in range(250, 360) and mouse_pointer[1] in range(400, 430):
                slot3 = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    result = check_slot('slot3.txt', 'slot3', (450,400))
                    game_mode = result[0]
                    done = result[1]
            elif mouse_pointer[0] in range(250, 350) and mouse_pointer[1] in range(500, 530):
                back = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    page = "front"
                    pygame.time.wait(200)
            
            create_button(background, "Slot 1", (250, 200), 40, slot1)
            create_button(background, "Slot 2", (250, 300), 40, slot2)
            create_button(background, "Slot 3", (250, 400), 40, slot3)
            create_button(background, "Back", (250, 500), 40, back)

    ########################## Create screen
            
        screen.blit(background, [0, 0])
        pygame.display.flip()
        
    ########################## Fps timer end
        
        endtime = time()

    ########################## Quit

    pygame.quit()
    pygame.freetype.quit()
    
    ########################## Return the value
    
    return game_mode