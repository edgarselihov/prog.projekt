Taavet Jukola, rühm AT3, juhendaja Kert Tali
Edgar Selihov, rühm INF4, juhendaja Andri Soone

1) Käigu põhine 2D võitlusmäng. Mängija võitleb AI-ga. Võites saab kogemuspunkte, millega arendab ennast. Avatari taseme põhjal genereeritakse pidevalt uusi vastaseid. Mäng kestab kuni mängija kaotab võitluse. 
Esimese etapi lõpuks võiks olla loodud esialgne graafiline disain ja esialge liikumis mehaanika.

