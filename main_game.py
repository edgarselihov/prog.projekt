import pygame
import pygame.time
import pygame.freetype
import first_menu
import Combat
from time import time
from ast import literal_eval

pygame.init()

def main(game_mode):
    
    ########################## Screen setup
    
    width = 1400
    height = 650
    resulutsioon = (width, height)
    screen = pygame.display.set_mode(resulutsioon, pygame.RESIZABLE)
    pygame.display.set_caption("Swordgame")
    background_original = pygame.image.load("Taust tegelastega.png").convert()
    
    ########################## new game stats
        
    if game_mode == "new game":
       player_stats = {'Level': 1, 'Skillpoints': 5, 'HP': 10, 'Current HP': 10, 'Energy': 30, \
                        'Current energy': 30, 'Strenght': 1, 'Attack': 1, 'Defence': 1}
       ########################## Enemy stats
    
       AI_stats = {'Level': 0, 'Skillpoints': 0, 'HP': 10, 'Current HP': 10, 'Energy': 30, \
                    'Current energy': 30, 'Strenght': 1, 'Attack': 1, 'Defence': 1}
    
       game_status = 'paused'
       
    ########################## Load stats
    
    elif game_mode == 'slot1':
        f = open('slot1.txt')
        player_stats = literal_eval(f.readline().strip('\n'))
        AI_stats = literal_eval(f.readline().strip('\n'))
        f.close()
        game_status = 'paused'
        
    elif game_mode == 'slot2':
        f = open('slot2.txt')
        player_stats = literal_eval(f.readline().strip('\n'))
        AI_stats = literal_eval(f.readline().strip('\n'))
        f.close()
        game_status = 'paused'
    
    elif game_mode == 'slot3':
        f = open('slot3.txt')
        player_stats = literal_eval(f.readline().strip('\n'))
        AI_stats = literal_eval(f.readline().strip('\n'))
        f.close()
        game_status = 'paused'

    ########################## Loop parameters and loop
    
    done = False
    endtime = 1
    starttime = 0
    saving = False
    move = None
    combat_info = None
    AI_info = None
    text_timer = 0
    
    while not done:
        
        ########################## Fps calc + Fps timer start
        
        fps = round(1/(endtime - starttime), 2)
        starttime = time()
        
        ########################## Where to write text
        
        background = pygame.Surface.copy(background_original)
        
        ########################## Show fps
        
        if fps > 60:
            pygame.time.Clock().tick(60)
            fps = 60
        first_menu.create_button(background, str(fps), (5, 5), 10)
        
        ########################## To quit
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.time.wait(200)
                screen = pygame.display.set_mode((650,650), pygame.RESIZABLE) # 
                game_mode = 'quit'
                done = True
        
        ########################## Menu and save button
        
        menu = False
        save = False
        mouse_pointer = pygame.mouse.get_pos()
        if mouse_pointer[0] in range(1160, 1200) and mouse_pointer[1] in range(20, 30):
            menu = True
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                game_mode = "menu"
                done = True
                pygame.time.wait(200)
        elif mouse_pointer[0] in range(1300, 1330) and mouse_pointer[1] in range(20, 30):
            save = True
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                saving = True
        first_menu.create_button(background, 'Menu', (1160, 20), 15, menu)
        first_menu.create_button(background, 'Save', (1300, 20), 15, save)
        
        ########################## Close save menu
        
        if mouse_pointer[0] in range(0, 1100):
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                saving = False
        
        ########################## Save buttons
        
        if saving == True:
            
            slot1 = False
            slot2 = False
            slot3 = False
            
            if mouse_pointer[0] in range(1200, 1280) and mouse_pointer[1] in range(200, 225):
                slot1 = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    f = open('slot1.txt', 'w+')
                    f.write(str(player_stats)+'\n')
                    f.write(str(AI_stats))
                    f.close()
                    
            elif mouse_pointer[0] in range(1200, 1280) and mouse_pointer[1] in range(300, 325):
                slot2 = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    f = open('slot2.txt', 'w+')
                    f.write(str(player_stats)+'\n')
                    f.write(str(AI_stats))
                    f.close()
                    
            elif mouse_pointer[0] in range(1200, 1280) and mouse_pointer[1] in range(400, 425):
                slot3 = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    f = open('slot3.txt', 'w+')
                    f.write(str(player_stats)+'\n')
                    f.write(str(AI_stats))
                    f.close()
            
            first_menu.create_button(background, 'Save to:', (1150, 100), 30)
            first_menu.create_button(background, 'Slot 1', (1200, 200), 30, slot1)
            first_menu.create_button(background, 'Slot 2', (1200, 300), 30, slot2)
            first_menu.create_button(background, 'Slot 3', (1200, 400), 30, slot3)
        
        ########################## Enemy info
        
        elif saving == False:
            first_menu.create_button(background, 'Level:', (1130, 120), 20)
            first_menu.create_button(background, 'HP:', (1130, 150), 20)
            first_menu.create_button(background, 'Energy:', (1130, 180), 20)
            first_menu.create_button(background, 'Strength:', (1130, 210), 20)
            first_menu.create_button(background, 'Attack:', (1130, 240), 20)
            first_menu.create_button(background, 'Defence:', (1130, 270), 20)
            
            first_menu.create_button(background, str(AI_stats['Level']), (1240, 120), 20)
            first_menu.create_button(background, str(AI_stats['Current HP'])+'/'+str(AI_stats['HP']), (1240, 150), 20)
            first_menu.create_button(background, str(AI_stats['Current energy'])+'/'+str(AI_stats['Energy']), (1240, 180), 20)
            first_menu.create_button(background, str(AI_stats['Strenght']), (1240, 210), 20)
            first_menu.create_button(background, str(AI_stats['Attack']), (1240, 240), 20)
            first_menu.create_button(background, str(AI_stats['Defence']), (1240, 270), 20)
        
        ########################## Player info
        
        first_menu.create_button(background, 'Player', (50, 50), 20, True)
        
        first_menu.create_button(background, 'Skillpoints:', (50, 80), 20)
        first_menu.create_button(background, 'Level:', (50, 120), 20)
        first_menu.create_button(background, 'HP:', (50, 150), 20)
        first_menu.create_button(background, 'Energy:', (50, 180), 20)
        first_menu.create_button(background, 'Strength:', (50, 210), 20)
        first_menu.create_button(background, 'Attack:', (50, 240), 20)
        first_menu.create_button(background, 'Defence:', (50, 270), 20)
        
        first_menu.create_button(background, str(player_stats['Skillpoints']), (160, 80), 20)
        first_menu.create_button(background, str(player_stats['Level']), (160, 120), 20)
        first_menu.create_button(background, str(player_stats['Current HP'])+'/'+str(player_stats['HP']), (160, 150), 20)
        first_menu.create_button(background, str(player_stats['Current energy'])+'/'+str(player_stats['Energy']), (160, 180), 20)
        first_menu.create_button(background, str(player_stats['Strenght']), (160, 210), 20)
        first_menu.create_button(background, str(player_stats['Attack']), (160, 240), 20)
        first_menu.create_button(background, str(player_stats['Defence']), (160, 270), 20)

        ########################## Combat buttons
        if game_status == 'running':
            if text_timer == 0:
                Attack_weak = False
                Attack_strong = False
                Counter = False
                Rest = False
                
                if mouse_pointer[0] in range(400, 500) and mouse_pointer[1] in range(620, 635):
                    Attack_weak = True
                    if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                        move = 'attack weak'
                elif mouse_pointer[0] in range(600, 710) and mouse_pointer[1] in range(620, 635):
                    Attack_strong = True
                    if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                        move = 'attack strong'
                elif mouse_pointer[0] in range(800, 855) and mouse_pointer[1] in range(620, 635):
                    Counter = True
                    if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                        move = 'counter'
                elif mouse_pointer[0] in range(950, 980) and mouse_pointer[1] in range(620, 635):
                    Rest = True
                    if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                        move = 'rest'
                
                first_menu.create_button(background, 'Attack (weak)', (400, 620), 15, Attack_weak)
                first_menu.create_button(background, 'Attack (strong)', (600, 620), 15, Attack_strong)
                first_menu.create_button(background, 'Counter', (800, 620), 15, Counter)
                first_menu.create_button(background, 'Rest', (950, 620), 15, Rest)
                
                ########################## Combat calc
                
                if move != None:
                    pygame.time.wait(500)
                    combat_info = Combat.player_fight_calc(move, player_stats, AI_stats)
                    move = None            
        
        ########################## Combat result check
        
            if player_stats['Current HP'] == 0:
                game_status = 'over'
            elif AI_stats['Current HP'] == 0:
                player_stats['Level'] += 1
                player_stats['Skillpoints'] += 5
                game_status = 'paused'
        
        ########################## Log and AI move
        
            if combat_info == 'player moved':
                AI_info = Combat.AI_logic(player_stats, AI_stats)
                combat_info = 'AI log'
            elif combat_info == 'players turn':
                first_menu.create_button(background, 'Your turn!', (670, 180), 20, True)
            elif combat_info == 'cant weak':
                first_menu.create_button(background, 'No energy for Attack(weak)', (580, 180), 20, True)
            elif combat_info == 'cant strong':
                first_menu.create_button(background, 'No energy for Attack(strong)', (580, 180), 20, True)
            elif combat_info == 'cant counter':
                first_menu.create_button(background, 'No energy for counter', (620, 180), 20, True)
            elif combat_info == 'cant rest':
                first_menu.create_button(background, "Can't rest, already at full energy", (590, 180), 20, True)
            elif combat_info == 'blocked':
                first_menu.create_button(background, "Enemy blocked your attack", (610, 180), 20, True)
                text_timer += 1
                if text_timer > 90:
                    combat_info = 'player moved'
                    text_timer = 1
            elif combat_info == 'bad counter':
                first_menu.create_button(background, "Unsuccessful counter", (610, 180), 20, True)
                text_timer += 1
                if text_timer > 90:
                    combat_info = 'player moved'
                    text_timer = 1
            elif combat_info == 'AI log':
                    if AI_info == 'blocked':
                        first_menu.create_button(background, "You blocked the opponents attack", (590, 180), 20, True)
                        text_timer += 1
                        if text_timer > 90:
                            combat_info = 'players turn'
                            text_timer = 0
                            AI_info = None
                    elif AI_info == 'bad counter':
                        first_menu.create_button(background, "Enemy countered poorly", (605, 180), 20, True)
                        text_timer += 1
                        if text_timer > 90:
                            combat_info = 'players turn'
                            text_timer = 0
                            AI_info = None
                    else:
                        text_timer = 0
                        combat_info = 'players turn'
                
        ########################## Combat result check 2
        
            if game_status == 'running':
                if player_stats['Current HP'] == 0:
                    game_status = 'over'
                elif AI_stats['Current HP'] == 0:
                    player_stats['Level'] += 1
                    player_stats['Skillpoints'] += 5
                    game_status = 'paused'
                
        ########################## Game over screen
                
        elif game_status == 'over':
            first_menu.create_button(background, 'YOU LOST', (620, 180), 50, True)
            
        ########################## level up buttons
            
        elif game_status == 'paused':
            plus_HP = False
            plus_Energy = False
            plus_Strenght = False
            plus_Attack = False
            plus_Defence = False
            
            if mouse_pointer[0] in range(246, 261) and mouse_pointer[1] in range(145, 163):
                plus_HP = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    player_stats['Skillpoints'] -=1
                    player_stats['HP'] += 1
                    pygame.time.wait(200)
            elif mouse_pointer[0] in range(246, 261) and mouse_pointer[1] in range(175, 193):
                plus_Energy = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    player_stats['Skillpoints'] -=1
                    player_stats['Energy'] += 1
                    pygame.time.wait(200)
            elif mouse_pointer[0] in range(246, 261) and mouse_pointer[1] in range(205, 223):
                plus_Strenght = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    player_stats['Skillpoints'] -=1
                    player_stats['Strenght'] += 1
                    pygame.time.wait(200)
            elif mouse_pointer[0] in range(246, 261) and mouse_pointer[1] in range(235, 253):
                plus_Attack = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    player_stats['Skillpoints'] -=1
                    player_stats['Attack'] += 1
                    pygame.time.wait(200)
            elif mouse_pointer[0] in range(246, 261) and mouse_pointer[1] in range(265, 283):
                plus_Defence = True
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    player_stats['Skillpoints'] -=1
                    player_stats['Defence'] += 1
                    pygame.time.wait(200)
                    
            first_menu.create_button(background, '+', (250, 150), 20, plus_HP)
            first_menu.create_button(background, '+', (250, 180), 20, plus_Energy)
            first_menu.create_button(background, '+', (250, 210), 20, plus_Strenght)
            first_menu.create_button(background, '+', (250, 240), 20, plus_Attack)
            first_menu.create_button(background, '+', (250, 270), 20, plus_Defence)
            
            if player_stats['Level'] != 1:
                first_menu.create_button(background, 'You Survived!', (670, 155), 20, True)
            
            first_menu.create_button(background, 'Prepare for the next fight!', (600, 180), 20, True)
            
            ########################## To be sure
            
            AI_stats['Current HP'] = 0
            
            ########################## Restart match
            
            if player_stats['Skillpoints'] == 0:
                game_status = 'running'
                combat_info = None
                ########################## Generate new enemy
                Combat.enemy_stats(player_stats, AI_stats)
                ########################## restore stats
                AI_stats['Current HP'] = AI_stats['HP']
                AI_stats['Current energy'] = AI_stats['Energy']
                player_stats['Current HP'] = player_stats['HP']
                player_stats['Current energy'] = player_stats['Energy']
        
        ########################## Create screen
        #print(mouse_pointer)
        
        screen.blit(background, [0, 0])
        pygame.display.flip()
        
        ########################## Fps timer end
        
        endtime = time()
        
    pygame.quit()
    pygame.freetype.quit()
    
    return game_mode