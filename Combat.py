
import pygame.time
from random import randint, choice
import first_menu

def attack_weak(strenght, attack, enemy_def):
    attack_skill = randint(0, (attack + 2))
    defence_skill = randint(0, (enemy_def + 2))
    if attack_skill > defence_skill:
        damage = int(3 + 0.4 * strenght)
        return damage
    else:
        return "blocked"

def attack_strong(strenght, attack, enemy_def):
    attack_skill = randint(0, (attack + 2))
    defence_skill = randint(0, (enemy_def + 6))
    if attack_skill > defence_skill:
        damage = int(5 + 0.7 * strenght)
        return damage
    else:
        return "blocked"

def move_counter(defence, strenght, enemy_attack):
    defence_skill = randint(0, (defence + 3))
    attack_skill = randint(0, enemy_attack)
    if defence_skill > attack_skill:
        damage = int(0.2 * strenght)
        return damage
    else:
        return "bad counter"

def move_rest(max_energy, max_hp):
    energy = int(max_energy/3)
    hp = int(max_hp/7)
    return (energy, hp)

def player_fight_calc(move, player_stats, AI_stats):
    if move == 'attack weak':
        if player_stats['Current energy'] >= 5:
            player_stats['Current energy'] -= 5
            damage = attack_weak(player_stats['Strenght'], player_stats['Attack'], AI_stats['Defence'])
            if damage == 'blocked':
                return "blocked"
            else:
                AI_stats['Current HP'] -= damage
                if AI_stats['Current HP'] < 0:
                    AI_stats['Current HP'] = 0
                return 'player moved'
        else:
            return 'cant weak'
        
    if move == 'attack strong':
        if player_stats['Current energy'] >= 10:
            player_stats['Current energy'] -= 10
            damage = attack_strong(player_stats['Strenght'], player_stats['Attack'], AI_stats['Defence'])
            if damage == 'blocked':
                return "blocked"
            else:
                AI_stats['Current HP'] -= damage
                if AI_stats['Current HP'] < 0:
                    AI_stats['Current HP'] = 0
                return 'player moved'
        else:
            return 'cant strong'
    
    if move == 'counter':
        if player_stats['Current energy'] >= 5:
            player_stats['Current energy'] -= 5
            damage = move_counter(player_stats['Defence'], player_stats['Strenght'], AI_stats['Attack'])
            if damage == 'bad counter':
                player_stats['Current HP'] -= 1
                return 'bad counter'
            else:
                AI_stats['Current HP'] -= damage
                player_stats['Current energy'] += 7
                if player_stats['Current energy'] > player_stats['Energy']:
                    player_stats['Current energy'] = player_stats['Energy']
                if AI_stats['Current HP'] < 0:
                    AI_stats['Current HP'] = 0
                return 'player moved'
        else:
            return 'cant counter'
    
    if move == 'rest':
        if player_stats['Current energy'] < player_stats['Energy']:   
            gain = move_rest(player_stats['Energy'], player_stats['HP'])
            
            player_stats['Current energy'] += gain[0]
            if player_stats['Current energy'] > player_stats['Energy']:
                player_stats['Current energy'] = player_stats['Energy']
            
            player_stats['Current HP'] += gain[1]
            if player_stats['Current HP'] > player_stats['HP']:
                player_stats['Current HP'] = player_stats['HP']
            return 'player moved'
        else:
            return 'cant rest'
       
def AI_logic(player_stats, AI_stats):
    ######## Possible moves
    options = []
    if AI_stats['Current energy'] < AI_stats['Energy']:
        times = int((AI_stats['Energy'] - AI_stats['Current energy'])/10)
        for i in range(times):
            options += ['rest']
            
    if AI_stats['Current energy'] >= 5:
        for i in range(5):
            options += ['attack weak']
        
        if (AI_stats['Defence'] + 3) > player_stats['Attack']:
            for i in range(5):
                options += ['counter']
        else:
            options += ['counter']
            
    if AI_stats['Current energy'] >= 10:
        if (AI_stats['Attack']-player_stats['Defence']) >= 3:
            for i in range(5):
                options += ['attack strong']
        else:
            options += ['attack strong']
    ###### Calculation
    AI_move = choice(options)
    options.clear() # reset list
    if AI_move == 'rest':
        gain = move_rest(AI_stats['Energy'], AI_stats['HP'])
        
        AI_stats['Current energy'] += gain[0]
        if AI_stats['Current energy'] > AI_stats['Energy']:
            AI_stats['Current energy'] = AI_stats['Energy']
        
        AI_stats['Current HP'] += gain[1]
        if AI_stats['Current HP'] > AI_stats['HP']:
            AI_stats['Current HP'] = AI_stats['HP']
    
    if AI_move == 'attack weak':
        AI_stats['Current energy'] -= 5
        damage = attack_weak(AI_stats['Strenght'], AI_stats['Attack'], player_stats['Defence'])
        if damage != 'blocked':
            player_stats['Current HP'] -= damage
            if player_stats['Current HP'] < 0:
                player_stats['Current HP'] = 0
        else:
            return 'blocked'
        
    if AI_move == 'attack strong':
        AI_stats['Current energy'] -= 10
        damage = attack_strong(AI_stats['Strenght'], AI_stats['Attack'], player_stats['Defence'])
        if damage != 'blocked':
            player_stats['Current HP'] -= damage
            if player_stats['Current HP'] < 0:
                player_stats['Current HP'] = 0
        else:
            return 'blocked'
    
    if AI_move == 'counter':
        AI_stats['Current energy'] -= 5
        damage = move_counter(AI_stats['Defence'], AI_stats['Strenght'], player_stats['Attack'])
        if damage != 'bad counter':
            player_stats['Current HP'] -= damage
            AI_stats['Current energy'] += 7
            if AI_stats['Current energy'] > AI_stats['Energy']:
                AI_stats['Current energy'] = AI_stats['Energy']
            if player_stats['Current HP'] < 0:
                player_stats['Current HP'] = 0
        else:
            AI_stats['Current HP'] -= 1
            return 'bad counter'
            
def enemy_stats(player_stats, AI_stats):
    AI_stats['HP'] = 10
    AI_stats['Energy'] = 30
    AI_stats['Strenght'] = 1
    AI_stats['Attack'] = 1
    AI_stats['Defence'] = 1
    AI_stats['Level'] = player_stats['Level']
    skillpoints = AI_stats['Level'] * 5
    stats = ["HP", "Energy", "Strenght", "Attack", "Defence"]
    while skillpoints > 0:
        upgrade = randint(1, 4)
        AI_stats[choice(stats)] += upgrade
        skillpoints -= upgrade
    if skillpoints < 0:
        AI_stats['Energy'] += skillpoints
        skillpoints = 0
    